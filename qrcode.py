import time, md5, base64, pyqrcode, qrtools, png, os, glob
from Crypto.Cipher import AES
from Crypto import Random
from pathlib import Path


# Pseudo global Initialization Vector, which my self-named master made

iv = [7, 129, 17, 49, 28, 73, 241, 99, 121, 21, 57, 75, 60, 14, 38, 9]

def GetTimeStamp():
    TimeStamp = int(time.time())
    TimeStamp = str(TimeStamp)
    return TimeStamp
	
def XorWithIV(input):
	result = []
	for x in range(0, len(input)):
		result.append(ord(input[x]) ^ IV[x])
	
	return "".join(chr(x) for x in result)
	
def SaveToFile(input):
	# Here we've got 16 length base64 code
    # Time to save it to QR code, using pyqrcode library
    _qr = pyqrcode.create(basedTicket, error='H', version=5, mode='binary')
    # Here i will get path to newest file, and create new
    newest = max(glob.iglob('*.[Pp][Nn][Gg]'), key=os.path.getctime)
    _qr_path = newest[:-4]
    _qr_file_type = newest[-4:]
    # Function to change name of image of qrcode
    def next_qr_code(_qr_path, _qr_file_type):
        my_file = Path(_qr_path + _qr_file_type)
        if my_file.is_file():
            i = int(_qr_path[-1:]) + 1
            i = str(i)
            a = _qr_path[:-1] + i
            a = str(a)
            return a + _qr_file_type
        else:
            a = 'qrcode0'
            return a
    # New path to create QR Code
    _qr_name = next_qr_code(_qr_path, _qr_file_type)
    # Save to PNG and show at the end
    _qr.png(_qr_name, scale=10)
    _qr.show()
	
	return _qr_name

def GenerateTicket():
    timeStamp = GetTimeStamp()
    md5hash = timeStamp + md5.new(timeStamp).hexdigest()[:6]
	
    encodedTicket = XorWithIV(md5hash)
    basedTicket = base64.b64encode(encodedTicket)
	
	filePath = SaveToFile(basedTicket)
	
	return filePath


# -------------------->END OF FUNCTION<-------------------------


# ----------->FUNCTION IS DECODING FROM QR CODE <---------------
def DECODE_QR_CODE(_qr_name):
    decode_it = qrtools.QR()
    decode_it.decode(_qr_name)
    return decode_it.data


# -------------------->END OF FUNCTION<-------------------------


# -------------------FUNCTION TO DECODE BASE64 FILE---------------
def BACK_TO_BEGINING(based_64, iv):
    output_value = base64.b64decode(based_64)

    #XORED function to UNXOR from based
    def strxor(IV, to_xor):

        xored = []
        for x in range(0, len(IV)):
            xored.append(IV[x]^ord(to_xor[x]))
        return "".join(chr(x) for x in xored)

    #SAM TIMESTAMP -> returning_value = strxor(iv, output_value)[-10:]
    returning_value = strxor(iv, output_value)
    return returning_value
# -------------->END OF FUNCTION<----------------------

#------------->FUNCTION TO CHECK TIMESTAMP<--------------
#If generated timestamp before printig is same as scannig
#returning true, else returning false
def checking_function(Global_TimeStamp, Returned_TimeStamp):
    if Global_TimeStamp == Returned_TimeStamp :
        return True
    else:
        return False

#------------------->END OF FUNCTION<--------------


def FUNKCJA_SKANUJACA(sciezka_do_pliku):
    based_64 = DECODE_QR_CODE(sciezka_do_pliku)
    return BACK_TO_BEGINING(based_64, iv)

def FUNKCJA_GENERUJACA():
    return GenerateTicket(iv, TimeStamp)

a = FUNKCJA_GENERUJACA()
print FUNKCJA_SKANUJACA(a)